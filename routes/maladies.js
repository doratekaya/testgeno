var express = require("express");
var router = express.Router();
const maladiecontroller=require('../controller/maladiecontroller')
const authenticate = require('../middleware/isAuthenticated')
router.get('/',maladiecontroller.index)
router.post('/addmaladie',maladiecontroller.store)
module.exports=router;