
  const mongoose = require('mongoose');
  //define schema
  
  const Schema= mongoose.Schema;
  
  const autoIncrementModelID = require('./counterModel');
  
  
  const userSchema = new Schema({
    
    id: { type: Number, unique: true, min: 1 },
  
    firstName: String,
    lastName:String,
    gender:String,
    date_birth:String,
    tel:String,
    maladie:String,
    age:String,
    image:String,
    idfamille :String,
     objectid:String,
    risque: 0.0,
    niveau: String,
  });
  userSchema.pre('save', function (next) {
    if (!this.isNew) {
      next();
      return;
    }
    autoIncrementModelID('user', this, next);
  });
  const user = mongoose.model('user', userSchema);
  module.exports= user ;
  